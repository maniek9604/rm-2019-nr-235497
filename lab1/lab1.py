import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
import random
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import math
from matplotlib.colors import LogNorm

d0 = 20
kp = 100
delta = 0.1
x_size = 10
y_size = 10

class Punkt:

    def __init__(self, X, Y):
        self.X = X
        self.Y = Y

def losujWspolrzednaY():
    w = random.randrange(-y_size, y_size)
    return w

def losujWspolrzednePrzeszkody():
    x = random.randrange(-x_size, x_size)
    y = random.randrange(-y_size, y_size)
    w = Punkt(x, y)
    return w

def Up(polozenie_robota, wspolrzedna_celu):

    odleglosc_x = polozenie_robota.X - wspolrzedna_celu.X
    odleglosc_y = polozenie_robota.Y - wspolrzedna_celu.Y

    norma = math.sqrt(math.pow(odleglosc_x, 2) + math.pow(odleglosc_y, 2))
    wartosc_Up = 0.5 * kp * norma ** 2
    return wartosc_Up

def V0i(polozenie_robota, współrzędne_przeszkody, k0i, i):

    odleglosc_x = polozenie_robota.X - współrzędne_przeszkody[i].X
    odleglosc_y = polozenie_robota.Y - współrzędne_przeszkody[i].Y
    norma = math.sqrt(math.pow(odleglosc_x, 2) + math.pow(odleglosc_y, 2))
    wartosc_V0i = 0
    if (norma <= d0):
        wartosc_V0i = 0.5 * k0i.get(i) * math.pow(((1 / norma) - (1 / d0)), 2)
    else:
        wartosc_V0i = 0
    print("LiczeV0i")
    return  wartosc_V0i

###############

def Fp(polozenie_robota, wspolrzedna_celu):
    wartosc_Fp = 0

    odleglosc_x = polozenie_robota.X - wspolrzedna_celu.X
    odleglosc_y = polozenie_robota.Y - wspolrzedna_celu.Y

    norma = math.sqrt(math.pow(odleglosc_x, 2) + math.pow(odleglosc_y, 2))
    wartosc_Fp = kp * norma
    return wartosc_Fp


def Foi(polozenie_robota, współrzędne_przeszkody, i, d0, k0i):

    wartosc_Foi = 0
    odleglosc_x = polozenie_robota.X - współrzędne_przeszkody[i].X
    odleglosc_y = polozenie_robota.Y - współrzędne_przeszkody[i].Y
    norma = math.sqrt(math.pow(odleglosc_x, 2) + math.pow(odleglosc_y, 2))

    if (norma == 0):
        return math.inf
    if (norma <= d0):
        wartosc_Foi = -k0i[i] * ((1 / norma) - (1 / d0)) * (1 / (norma ** 2))
    else:
        wartosc_Foi = 0
    return wartosc_Foi


def LiczSile(polozenie_robota, wspolrzedna_celu, współrzędne_przeszkody, k0i):
    wartosc = 0
    for i in range(0, 4):
        wartosc += Foi(polozenie_robota, współrzędne_przeszkody, i, d0, k0i)
    Fw = Fp(polozenie_robota, wspolrzedna_celu) - wartosc
    return Fw



# obst_vect = [Punkt(1, 3.5),Punkt(-2.5, 4.0), Punkt(4, 9), Punkt(1, 3)]
obst_vect = [losujWspolrzednePrzeszkody(),losujWspolrzednePrzeszkody(), losujWspolrzednePrzeszkody(), losujWspolrzednePrzeszkody()]
#start_point = Punkt(-10, 3)
start_point = Punkt(-10, losujWspolrzednaY())

#finish_point = Punkt(10, 3)
finish_point = Punkt(10, losujWspolrzednaY())

pozycja_robota = Punkt(0, 0)
wspolczynnikiPrzeszkod = [1, 1, 1, 1]


# szukamy z dokladnoscia do 0.1  20*0.1 = 200 + 1 bo liczymy od 0

Sila = [[0 for x in range(201)] for y in range(201)]

for y in range(0, 201):
    for x in range(0, 201):
        pozycja_robota.X = -x_size + x * (1/10)
        pozycja_robota.Y = -y_size + y * (1/10)
        Sila[y][x] = LiczSile(pozycja_robota, finish_point, obst_vect, wspolczynnikiPrzeszkod)
        print("y=", pozycja_robota.Y, ",x=", pozycja_robota.X, ", s=", Sila[y][x])



x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = Sila

fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size])



plt.plot(start_point.X, start_point.Y, "or", color='blue')
plt.plot(finish_point.X, finish_point.Y, "or", color='blue')
plt.plot(pozycja_robota.X, pozycja_robota.Y, "or", color='red')

for obstacle in obst_vect:
    plt.plot(obstacle.X, obstacle.Y, "or", color='black')

plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()






