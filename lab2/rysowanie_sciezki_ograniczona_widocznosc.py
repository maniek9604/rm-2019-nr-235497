# Program rysujacy sciezke robota - branie pod uwage preszkod tylko w pewnej odleglosci



import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
import random
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import math
from matplotlib.colors import LogNorm


liczba_przeszkod = 20   # ustawiamy liczbe przeszkod
dystans_do_przeszkod = 5 # robot reaguje tylko na przeszkody mieszczace sie w promnieniu zasiegu robota
d0 = 20
kp = 1
delta = 0.5
x_size = 10
y_size = 10

class Punkt:

    def __init__(self, X, Y):
        self.X = X
        self.Y = Y

def losujWspolrzednaY():
    w = random.randrange(-y_size, y_size)
    print("Wspolrzedna W =", w)
    return w

def losujWspolrzednePrzeszkody():
    x = random.randrange(-x_size, x_size)
    y = random.randrange(-y_size, y_size)
    print("Wspolrzedna Przeskody X =", x, "Y:",y)
    w = Punkt(x, y)
    return w

def Up(polozenie_robota, wspolrzedna_celu):

    odleglosc_x = polozenie_robota.X - wspolrzedna_celu.X
    odleglosc_y = polozenie_robota.Y - wspolrzedna_celu.Y

    norma = math.sqrt(math.pow(odleglosc_x, 2) + math.pow(odleglosc_y, 2))
    wartosc_Up = 0.5 * kp * norma ** 2
    return wartosc_Up

def V0i(polozenie_robota, współrzędne_przeszkody, k0i, i):

    odleglosc_x = polozenie_robota.X - współrzędne_przeszkody[i].X
    odleglosc_y = polozenie_robota.Y - współrzędne_przeszkody[i].Y
    norma = math.sqrt(math.pow(odleglosc_x, 2) + math.pow(odleglosc_y, 2))
    wartosc_V0i = 0
    if (norma <= d0):
        wartosc_V0i = 0.5 * k0i.get(i) * math.pow(((1 / norma) - (1 / d0)), 2)
    else:
        wartosc_V0i = 0
    print("LiczeV0i")
    return  wartosc_V0i

###############

def Fp(polozenie_robota, wspolrzedna_celu):
    wartosc_Fp = 0

    odleglosc_x = polozenie_robota.X - wspolrzedna_celu.X
    odleglosc_y = polozenie_robota.Y - wspolrzedna_celu.Y

    norma = math.sqrt(math.pow(odleglosc_x, 2) + math.pow(odleglosc_y, 2))
    wartosc_Fp = kp * norma
    return wartosc_Fp


def Foi(polozenie_robota, współrzędne_przeszkody, i, d0, k0i):

    wartosc_Foi = 0
    odleglosc_x = polozenie_robota.X - współrzędne_przeszkody[i].X
    odleglosc_y = polozenie_robota.Y - współrzędne_przeszkody[i].Y
    norma = math.sqrt(math.pow(odleglosc_x, 2) + math.pow(odleglosc_y, 2))

    if (norma == 0):
        return math.inf
    if (norma <= d0):
        wartosc_Foi = -k0i[i] * ((1 / norma) - (1 / d0)) * (1 / (norma ** 2))
    else:
        wartosc_Foi = 0
    return wartosc_Foi


def LiczSile(polozenie_robota, wspolrzedna_celu, współrzędne_przeszkody, k0i):
    wartosc = 0
    for i in range(0, liczba_przeszkod):
        wartosc += Foi(polozenie_robota, współrzędne_przeszkody, i, d0, k0i)
    Fw = Fp(polozenie_robota, wspolrzedna_celu) + wartosc
    return Fw


#obst_vect = [Punkt(1, 3.5),Punkt(-2.5, 4.0), Punkt(4, 6), Punkt(1, 3)]

# losowanie przeszkod
obst_vect = []
for i in range(0, liczba_przeszkod):
    obst_vect.append(losujWspolrzednePrzeszkody())

#obst_vect = [losujWspolrzednePrzeszkody(),losujWspolrzednePrzeszkody(), losujWspolrzednePrzeszkody(), losujWspolrzednePrzeszkody()]
#start_point = Punkt(-10, 3)
start_point = Punkt(-10, losujWspolrzednaY())

#finish_point = Punkt(10, 3)
finish_point = Punkt(10, losujWspolrzednaY())

pozycja_robota = Punkt(0, 0)
przeszkoda = Punkt(0, 0)

cel = Punkt(0, 0)
wypadkowa = Punkt(0,0)


wspolczynnikiPrzeszkod = []
for i in range(0, liczba_przeszkod):
    wspolczynnikiPrzeszkod.append(1)


# szukamy z dokladnoscia do 0.1  20*0.1 = 200 + 1 bo liczymy od 0

Sila = [[0 for x in range(201)] for y in range(201)]
Trasa = [[0 for x in range(201)] for y in range(201)]

for y in range(0, 201):
    for x in range(0, 201):
        pozycja_robota.X = -x_size + x * (1/10)
        pozycja_robota.Y = -y_size + y * (1/10)
        Sila[y][x] = LiczSile(pozycja_robota, finish_point, obst_vect, wspolczynnikiPrzeszkod)
        #print("y=", pozycja_robota.Y, ",x=", pozycja_robota.X, ", s=", Sila[y][x])


for y in range(0, 201):
    for x in range(0, 201):
        sila = 0
        dist = 0
        wypadkowa.X = 0
        wypadkowa.Y = 0
        pozycja_robota.X = -x_size + x * (1/10)
        pozycja_robota.Y = -y_size + y * (1/10)
        for i in range(0, liczba_przeszkod):
            dist = math.sqrt(
                math.pow((pozycja_robota.X - obst_vect[i].X), 2) + math.pow((pozycja_robota.Y - obst_vect[i].Y), 2))
            if (dist != 0 and dist < dystans_do_przeszkod):
                przeszkoda.X = (obst_vect[i].X-pozycja_robota.X)/dist
                przeszkoda.Y = (obst_vect[i].Y - pozycja_robota.Y)/dist
                sila = -wspolczynnikiPrzeszkod[i] * ((1 / dist) - (1 / d0)) * (1 / (dist ** 2))
                przeszkoda.X = przeszkoda.X * sila
                przeszkoda.Y = przeszkoda.Y * sila

            wypadkowa.X += przeszkoda.X
            wypadkowa.Y += przeszkoda.Y

        odleglosc_do_celu = math.sqrt(math.pow((pozycja_robota.X - finish_point.X), 2) + math.pow((pozycja_robota.Y - finish_point.Y), 2))

        if( odleglosc_do_celu != 0):
            cel.X = (finish_point.X - pozycja_robota.X)/odleglosc_do_celu
            cel.Y = (finish_point.Y - pozycja_robota.Y)/odleglosc_do_celu
            sila_do_celu = kp * odleglosc_do_celu
            cel.X = cel.X * sila_do_celu
            cel.Y = cel.Y * sila_do_celu

        wypadkowa.X = wypadkowa.X + cel.X
        wypadkowa.Y = wypadkowa.Y + cel.Y
        Trasa[y][x] = math.atan2(wypadkowa.Y, wypadkowa.X)
       # print("y=", pozycja_robota.Y, ",x=", pozycja_robota.X, "Y=", y, "X=", x,  ", s=", Trasa[y][x])

x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = Sila

fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda sil')
plt.imshow(Z, cmap=cm.RdYlGn,vmin = -1,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size])


plt.plot(start_point.X, start_point.Y, "or", color='blue')
plt.plot(finish_point.X, finish_point.Y, "or", color='blue')

pozycja_robota.X = start_point.X
pozycja_robota.Y = start_point.Y

plt.plot(pozycja_robota.X, pozycja_robota.Y, "or", color='red')

i = 0
while math.fabs((pozycja_robota.X - finish_point.X)) > 0.01 or math.fabs(pozycja_robota.Y -finish_point.Y) > 0.01:
    i = i+1
    angle = Trasa[int((pozycja_robota.Y*10+100))][int((pozycja_robota.X*10+100))]

    # ostatni punkt czasem ma za mala sile i nie dochodzi do niego
    if(math.fabs((pozycja_robota.X - finish_point.X)) < 0.01):
        if( finish_point.Y > pozycja_robota.Y ):
            pozycja_robota.Y = pozycja_robota.Y + 0.1
        else:
            pozycja_robota.Y = pozycja_robota.Y - 0.1
    elif (angle <= (math.pi/8) and angle >= (-math.pi/8 )):
        pozycja_robota.X = pozycja_robota.X + 0.1
    elif (angle <= (3*math.pi/8) and angle > (math.pi/8 )):
        pozycja_robota.X = pozycja_robota.X + 0.1
        pozycja_robota.Y = pozycja_robota.Y + 0.1
    elif (angle <= (5*math.pi/8) and angle > (3* math.pi/8 )):
        pozycja_robota.Y = pozycja_robota.Y + 0.1
    elif (angle <= (7*math.pi/8) and angle > (5* math.pi/8 )):
        pozycja_robota.X = pozycja_robota.X - 0.1
        pozycja_robota.Y = pozycja_robota.Y - 0.1
    elif (angle > (7* math.pi/8 )):
        pozycja_robota.X = pozycja_robota.X - 0.1
    elif (angle < (-math.pi/8) and angle >= (-3* math.pi/8 )):
        pozycja_robota.X = pozycja_robota.X + 0.1
        pozycja_robota.Y = pozycja_robota.Y - 0.1
    elif (angle < (-3*math.pi/8) and angle >= (-5* math.pi/8 )):
        pozycja_robota.Y = pozycja_robota.Y - 0.1
    elif (angle < (-5*math.pi/8) and angle >= (-7* math.pi/8 )):
        pozycja_robota.X = pozycja_robota.X - 0.1
        pozycja_robota.Y = pozycja_robota.Y - 0.1
    elif (angle < (-7*math.pi/8)):
        pozycja_robota.X = pozycja_robota.X - 0.1

    pozycja_robota.X = round(pozycja_robota.X,2)
    pozycja_robota.Y = round(pozycja_robota.Y, 2)

    print("y=", pozycja_robota.Y, ",x=", pozycja_robota.X, "angle:" , round(angle,4))
    plt.plot(pozycja_robota.X, pozycja_robota.Y, "or", color='red')

    if (i > 500):
        print("Brak mozliwoscia dotarcia do punktu koncowego")
        break
for obstacle in obst_vect:
    plt.plot(obstacle.X, obstacle.Y, "or", color='black')

plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()






